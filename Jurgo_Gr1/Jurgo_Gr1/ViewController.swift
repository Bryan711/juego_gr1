//
//  ViewController.swift
//  Jurgo_Gr1
//
//  Created by BRYAN OCAÑA on 14/11/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Attributes
    let gameModel = GameModel()
    //MARK:-Outlet
    
    @IBOutlet weak var objetivoLabel: UILabel!
    @IBOutlet weak var puntajeLabel: UILabel!
    @IBOutlet weak var rondaLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    @IBAction func ReiniciarButtonPressed(_ sender: Any) {
        gameModel.reiniciar()
        setValores()
    }
    
    @IBAction func JugarButtonPressed(_ sender: Any) {
        gameModel.jugar(valorIntento: Int(round(slider.value)))
        setValores()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValores()
    }

    func setValores(){
        objetivoLabel.text = "\(gameModel.objetivo ?? 0)"
        puntajeLabel.text = "\(gameModel.puntaje)"
        rondaLabel.text = "\(gameModel.round)"
    }
    
    
}

