//
//  GameModel.swift
//  Jurgo_Gr1
//
//  Created by BRYAN OCAÑA on 14/11/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import Foundation

class GameModel{
    var puntaje:Int = 0
    var round:Int = 1
    var objetivo:Int?
    
    init() {
        setObjetivo()
    }
    
    public func setObjetivo(){
        objetivo = Int(arc4random_uniform(99) + 1)
    }
    
    public func reiniciar(){
        puntaje = 0
        round = 1
        setObjetivo()
    }
    
    func calcularPuntaje(valorIntento:Int) -> Int {
        let diferencia = abs(valorIntento - objetivo!)
        
        switch diferencia {
        case 0:
            return 100
        case 1...3:
            return 75
        case 4...10:
            return 50
        default:
            return 0
        }
    }
    
    func jugar(valorIntento:Int){
        round += 1
        puntaje += calcularPuntaje(valorIntento: valorIntento)
        setObjetivo()
    }
}
