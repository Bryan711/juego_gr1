//
//  GameModelTests.swift
//  Jurgo_Gr1Tests
//
//  Created by BRYAN OCAÑA on 14/11/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import XCTest
@testable import Jurgo_Gr1


class GameModelTests: XCTestCase {
    
    //escribir una prueba que falle
    
        var gameModel:GameModel?
        
        override func setUp(){
            gameModel = GameModel()
        }
        //vemos que el puntaje inical sea cero, declaramos en la clase el valor
       
    func testPuntajeInicialEsCero () {
        XCTAssertEqual(gameModel!.puntaje, 0)
    }
    
    func testRoundInicialEsUno(){
        XCTAssertEqual(gameModel!.round, 1)
    }
    
    func testSetObjetivoEsRandom(){
        gameModel!.setObjetivo()
        let objetivo = gameModel!.objetivo
        gameModel!.setObjetivo()
        let objetivo2 = gameModel!.objetivo
        XCTAssert(objetivo != objetivo2)
        XCTAssert(objetivo! > 0)
        XCTAssert(objetivo! < 100)}
    
    func testObjeticoEsNumero(){
        let Objetivo = gameModel!.objetivo
        XCTAssertNotNil(Objetivo)
    }
    
    func testJugarActualizaRonda(){
        let rondaActual = gameModel?.round
        gameModel!.jugar(valorIntento: 0)
        XCTAssertEqual(gameModel?.round, rondaActual! + 1)
        
    }
    
    func testReiniciarRondaUnoPuntajeCero(){
        gameModel!.puntaje = 1000
        gameModel!.round = 9
        
        gameModel!.reiniciar()
        
        XCTAssertEqual(gameModel!.puntaje, 0)
        XCTAssertEqual(gameModel!.round, 1)
        
    }
    
    func testCalcularPuntajeDiferenciaTres(){
        gameModel!.objetivo = 10
        let puntaje = gameModel!.calcularPuntaje(valorIntento: 8)
        XCTAssertEqual(puntaje, 75)
    }
    
    func testCalcularPuntajeDiferenciaDiez(){
        gameModel!.objetivo = 20
        let puntaje = gameModel!.calcularPuntaje(valorIntento: 10)
        let puntaje2 = gameModel!.calcularPuntaje(valorIntento: 30)
        
        XCTAssertEqual(puntaje, 50)
        XCTAssertEqual(puntaje2, 50)
    }
    
    func testCalcularPuntajeEsCero(){
        gameModel!.objetivo = 50
        let puntaje = gameModel!.calcularPuntaje(valorIntento: 20)
        let puntaje2 = gameModel!.calcularPuntaje(valorIntento: 70)
        
        XCTAssertEqual(puntaje, 0)
        XCTAssertEqual(puntaje2, 0)
        
    }
    
    func testJugaríncrementarPuntaje(){
        gameModel!.objetivo = 20
        gameModel!.jugar(valorIntento: 20)
        gameModel!.objetivo = 30
        gameModel!.jugar(valorIntento: 31)
        
        XCTAssertEqual(gameModel!.puntaje, 175)
    }
    
    func testObjetivoDaValorAObjetivo(){
        gameModel!.setObjetivo()
        let objetivo = gameModel!.objetivo
        XCTAssertEqual(gameModel!.objetivo, objetivo)
    }
    
    func testObjetivoCambiaALjugar(){
        let objetivo = gameModel!.setObjetivo()
        gameModel!.jugar(valorIntento: 100)
        let objetivo2 = gameModel!.setObjetivo()
        XCTAssert(objetivo != objetivo2)
        
    }
    
    func testObjetivoCambiarCuandoReinicia(){
        let objetivo = gameModel!.objetivo
        gameModel!.reiniciar()
        let objetivo2 = gameModel!.objetivo
        XCTAssert(objetivo != objetivo2)
    }
}














































